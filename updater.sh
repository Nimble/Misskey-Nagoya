#!/bin/sh
# You need to use doas corepack enable to activate pnpm

export NODE_ENV=production
#Update latest pnpm
corepack prepare pnpm@latest --activate
#yarn install
echo "Installing dependencies..."
pnpm install
#yarn build
echo "Building Misskey...============="
pnpm build
#yarn migrate
echo "Database migration............"
pnpm migrate

#echo "default permissions 100644 changed to -> 100755"
echo "You can enable the service now"
