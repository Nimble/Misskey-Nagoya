#!/bin/sh
echo "Stop misskey service since we will change user ownership."
doas systemctl stop misskey
wait

echo "This script changes the user ownership of the directory you are in while keeping the group it belongs to."
echo "The user that runs this script will be the owner (Needs proper use of $ HOME if you don't input any user)."

g=$(stat -c "%G" .)
u=$1

if [ -z "$u" ]
then
	u=$(stat -c "%U" $HOME)
	echo Changing user ownership to $u:$g
	doas chown $u:$g -R .
else
	echo Changing user ownership to $u:$g
	doas chown $u:$g -R .
fi

#TODO clean this mess of a command: (Use sudo su root conditionals etc.)

echo "Changed ownership... $(ls -la -hs pre-update.sh)"
